//Zadanie 37:
//        Stworzymy aplikację, która będzie stanowić rejestr produktów w sklepie.
//        Zakładamy istnienie małego sklepiku, w którym musimy raz na jakiś czas przeprowadzać inwentaryzację.
// Aby nie musieć spisywać wszystkich produktów do notatnika, czy też innego dokumentu (np. excel'a) napiszemy aplikację,
// która będzie dodawać produkty po ich wprowadzeniu w linii poleceń.


//        TYP - będzie enumem, będzie to typ produktu, nazwiemy go PRODUCT_TYPE. Wyróżniamy kilka podstawowych typów
// (jeśli znacie więcej, to prosze śmiało dodać): FOOD, INDUSTRIAL, ALCOHOL

//        Z_JAKIEJ_POLKI będzie kolejnym enumem, nazwiemy go PRODUCT_CLASS i będzie reprezentować typ klasy
// (np. coś z górnej półki, z średniej, z dolnej):
//        HIGH, MID, LOW

//        CENA jest ceną produktu wyrażoną w Double lub w Integer (zabezpiecz aplikację w taki sposób aby przyjmowała
// oba typy parametrów)

//        NAZWA_PRODUKTU to po prostu String.


public class Product {

    private PRODUCT_CLASS productClass;
    private PRODUCT_TYPE productType;
    private String productName;
    private double price;

    public Product(PRODUCT_CLASS productClass, PRODUCT_TYPE productType, String productName, double price) {
        this.productClass = productClass;
        this.productType = productType;
        this.productName = productName;
        this.price = price;
    }

    public PRODUCT_CLASS getProductClass() {
        return productClass;
    }

    public void setProductClass(PRODUCT_CLASS productClass) {
        this.productClass = productClass;
    }

    public PRODUCT_TYPE getProductType() {
        return productType;
    }

    public void setProductType(PRODUCT_TYPE productType) {
        this.productType = productType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
