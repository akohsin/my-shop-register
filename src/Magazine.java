import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Magazine {
    Map<PRODUCT_CLASS, List<Product>> kolekcja = new HashMap<>();
    Map<PRODUCT_TYPE, List<Product>>  kolekcja2 = new HashMap<>();

    public Magazine() {
        for (PRODUCT_CLASS x : PRODUCT_CLASS.values()) {
            kolekcja.put(x, new ArrayList<Product>());
        }
        for (PRODUCT_TYPE x : PRODUCT_TYPE.values()){
            kolekcja2.put(x, new ArrayList<Product>());
        }
    }


    public void addProductClass(Product product) {
        kolekcja.get(product.getProductClass()).add(product);
        kolekcja2.get(product.getProductType()).add(product);
    }
}
